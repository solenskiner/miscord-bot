#!/bin/bash

# to run your own testing instance of this bot
# you need a discord token.
# those can be had at: 
# https://discord.com/developers/applications
export discord_name=""
export discord_application_id=""
export discord_api_pubkey=""
export discord_token=""

# discord permissions are needed when adding
# a bot to a guild.
# calculate them using:
# https://discordapi.com/permissions.html
# at the bottom of the page is the link to add
# the bot to a guild
export discord_permissions=""

