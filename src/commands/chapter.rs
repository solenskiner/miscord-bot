use serenity::prelude::*;
use serenity::model::prelude::*;
use serenity::framework::standard::{
    CommandResult,
    macros::command,
};
use serenity::model::id::RoleId;
use std::string::ToString;
extern crate strum;
use strum::IntoEnumIterator;

// data from https://mensa.se/om-mensa/lokalomraden-mensa-sverige/
#[derive(Hash, Eq, PartialEq, Debug, Clone, Copy, EnumIter, Display)]
enum Chapter {
    #[strum(serialize="Mensa Stockholm")]
    MensaStockholm = 0,
    #[strum(serialize="Mensa Syd")]
    MensaSyd = 1,
    #[strum(serialize="Mensa Linné")]
    MensaLinne = 2,
    #[strum(serialize="LMA Kalmar")]
    LMAKalmar = 3,
    #[strum(serialize="Mensa Väst")]
    MensaVaast = 4,
    #[strum(serialize="Mensa Jönköping")]
    MensaJoenkoeping = 5,
    #[strum(serialize="Mensa Öst")]
    MensaOest = 6,
    #[strum(serialize="LMA Mälardalen")]
    LMAMaelardalen = 7,
    #[strum(serialize="LMA Värmland")]
    LMAVaermland = 8,
    #[strum(serialize="LMA Örebro")]
    LMAOerebro = 9,
    #[strum(serialize="Mensa Uppsala")]
    MensaUppsala = 10,
    #[strum(serialize="LMA Dalarna")]
    LMADalarna = 11,
    #[strum(serialize="LMA Gävleborg")]
    LMAGaevleborg = 12,
    #[strum(serialize="LMA Mittsverige")]
    LMAMittsverige = 13,
    #[strum(serialize="Mensa Nord")]
    MensaNord = 14,
}

const CHAPTERS_CODE_RANGES:[(std::ops::RangeInclusive<i32>, Chapter); 16] = [
    ({10..=19}, Chapter::MensaStockholm),
    ({20..=32}, Chapter::MensaSyd),
    ({33..=37}, Chapter::MensaLinne),
    ({38..=39}, Chapter::LMAKalmar),
    ({40..=51}, Chapter::MensaVaast),
    ({52..=57}, Chapter::MensaJoenkoeping),
    ({58..=62}, Chapter::MensaOest),
    ({63..=64}, Chapter::LMAMaelardalen),
    ({65..=68}, Chapter::LMAVaermland),
    ({69..=71}, Chapter::LMAOerebro),
    ({72..=73}, Chapter::LMAMaelardalen),
    ({74..=76}, Chapter::MensaUppsala),
    ({77..=79}, Chapter::LMADalarna),
    ({80..=82}, Chapter::LMAGaevleborg),
    ({83..=87}, Chapter::LMAMittsverige),
    ({89..=99}, Chapter::MensaNord),
];

extern crate custom_error;
use custom_error::custom_error;

custom_error!{MyError 
    NoGID                  = "No guildid on message",
    BadMatch { msg: String }   = "Message content did not match regex. msg: {}",
    BadChapter { code: i32 }   = "Chapter matching code not found. code {}",
    GIDCacheMiss { gid: GuildId }   = "Guildid not in cache. GuildId: {}",
    Serenity { source: serenity::Error } = "Serenity error: {}",
}
use MyError::*;
 
#[command]
async fn chapter(ctx: &Context, msg: &Message) -> CommandResult {
    /*let help ="Usage:
!chapter postalcode

makes your local chapter visible when clicking your username.
e.g: `!chapter 412 00` sets chapter to Mensa Väst,
and anyone mentioning @mensaväst will notify you";*/

    match (|| async {
        let caps = regex!("^!chapter #?([\\d]{2})[\\d][\\s-]?[\\dx]{2}$")
        .captures(&msg.content).and_then(|res| { res.get(1) })
        .ok_or_else(|| BadMatch { msg: msg.content.clone() })?;

        // if unwrap would be unsound, regex would have failed the parse
        let code = i32::from_str_radix(caps.as_str(), 10).unwrap();

        let chapter = 
            *CHAPTERS_CODE_RANGES.iter().find_map(|(range, chapter)| {
                if range.start() <= &code && &code <= range.end() {
                    return Some(chapter);
                }
                return None;
            }).ok_or_else(|| BadChapter{ code })?;

        let gid = msg.guild_id.ok_or_else(|| NoGID)?;
        let guild = ctx.cache.guild(gid).await.ok_or_else( || GIDCacheMiss{ gid: gid })?;
        let mut member = guild.member(ctx.http.clone(), msg.author.id).await?;

        let members_existing_role:Vec<RoleId> = 
            member.roles.iter().filter(|&member_role_id| {
                if let Some(member_role) = guild.roles.get(member_role_id) {
                    return Chapter::iter().find_map(|chapter| {
                        if chapter.to_string() == member_role.name { return Some(()) } else {  None }
                    }).is_some()
                } else {
                    return false;
                }
            }).map(|role_id| role_id.clone()).collect();
        if !members_existing_role.is_empty() {
            member.remove_roles(ctx.http.clone(), &members_existing_role).await?;
        } 

        if let Some(guild_existing_role) = 
            guild.roles.iter().find_map(|(_, guild_role)| {
                if chapter.to_string() == guild_role.name { Some(guild_role) } else { None }
                //unwrap is safe, since chapter came from vec chapter_role_names
            }) {
            member.add_role(ctx.http.clone(), guild_existing_role).await?;
        } else {
            let guild_new_role = guild.create_role(ctx.http.clone(), |r| {
                r.mentionable(true)
                .name(chapter.to_string())
                //unwrap is safe, since chapter came from vec chapter_role_names
            }).await?;
            member.add_role(ctx.http.clone(), guild_new_role).await?;
        }

        msg.react(ctx.http.clone(), '✅').await?;

        Ok(())
    })().await {
        Ok(_) => Ok(()),
/*        Err(BadChapter { code })        => { println!("BadChapter: {}", code); Ok(())},
        Err(BadMatch { msg })           => Ok(()), // prolly do something?
        Err(NoGID {})                        => Ok(()),  // msg came trough pm, drop?
        Err(GIDCacheMiss { gid })     => Ok(()),  // how could this fail?*/
        //Err(Serenity) => ??
        Err(error)         =>  {
            println!("{}", error);
            /*match error {
                x => {},
            };*/
            Err(MyError::into(error))
        }
    }
}
