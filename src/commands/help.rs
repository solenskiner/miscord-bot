use serenity::prelude::*;
use serenity::model::prelude::*;
use serenity::framework::standard::{
    CommandResult,
    macros::command,
};

#[command]
async fn help(ctx: &Context, msg: &Message) -> CommandResult {
    let help ="Valid commands:
!chapter postal_code -- makes your local chapter visible when clicking your username
!joined year         -- makes the year you joined mensa visible on when clicking your username
!color colorhex      -- set your color in chat
!help                -- this text";

    msg.reply(ctx, help).await?;

    Ok(())
}
