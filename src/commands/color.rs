use serenity::prelude::*;
use serenity::model::prelude::*;
use serenity::framework::standard::{
    CommandResult,
    macros::command,
};

#[command]
async fn color(ctx: &Context, msg: &Message) -> CommandResult {
    let help = "Usage:
!color colorhex      -- set your color in chat. e.g: !color #ffffff, use https://www.color-hex.com/ or something";

    if let Some(caps) = regex!("^!color #?([0-9a-foirstg]{6})$")
    .captures(&msg.content)
    .and_then(|res| {res.get(1)}) {
        if let Some(gid) = msg.guild_id {
            if let Some(guild) = ctx.cache.guild(gid).await {

                let leet_string:String = caps.as_str().to_lowercase().chars().map(|c| match c {
                    'o' => '0',
                    'i' => '1',
                    'r' => '2',
                    's' => '5',
                    't' => '7',
                    'g' => '9',
                    _ => c,
                }).collect();

                // if unwrap would be unsound, regex would have failed the parse.
                let colour = u64::from_str_radix(leet_string.as_str(), 16).unwrap();

                if let Some(previous_role) = guild.roles.values().find(|&role| {
                    role.name == msg.author.tag()
                }) {
                    previous_role.edit(ctx.http.clone(), |r| {
                        r.colour(colour)
                    }).await?;
                } else {
                    let new_role = guild.create_role(ctx.http.clone(), |r| {
                        r.mentionable(false)
                        .name(msg.author.tag())
                        .colour(colour)
                    }).await?;
                    let mut member = guild.member(ctx.http.clone(), msg.author.id).await?;
                    member.add_role(ctx.http.clone(), new_role).await?;
                }

                msg.react(ctx.http.clone(), '✅').await?;
            }
        } else {
            // command came trough pm maybe? has no guildid. prolly do something?
        }
    } else {
        msg.reply(ctx, help).await?;
    };

    Ok(())
}