use serenity::prelude::*;
use serenity::model::prelude::*;
use serenity::framework::standard::{
    CommandResult,
    macros::command,
};

#[command]
async fn joined(ctx: &Context, msg: &Message) -> CommandResult {
    let help ="";

    msg.reply(ctx, help).await?;

    Ok(())
}
